const firstName = 'Ahmad';
const middleName = "Zaki";
const lastName = "Yamani";

const sum = (first, second) => {
  return `hasil : ${first + second}`;
};

class Person {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  sayHello = () => {
    console.log(`hy ${this.firstName} ${this.lastName}`);
  };
}

export { firstName, middleName, lastName, sum, Person };
